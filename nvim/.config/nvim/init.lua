table.unpack = rawget(table, "unpack") or unpack
vim.uv = rawget(vim, "uv") or rawget(vim, "loop")

vim.loader.enable()

local function bootstrap(url)
	local name = url:gsub(".*/", "")
	local path = vim.fn.stdpath "data" .. "/lazy/" .. name

	if vim.fn.isdirectory(path) == 0 then
		print(name .. ": installing in data dir...")

		vim.fn.system { "git", "clone", url, path }

		vim.cmd "redraw"
		print(name .. ": finished installing")
	end

	vim.opt.rtp:prepend(path)
end

bootstrap "https://github.com/udayvir-singh/tangerine.nvim"
bootstrap "https://github.com/udayvir-singh/hibiscus.nvim"

require "tangerine".setup {
	compiler = {
		hooks = { "oninit", "onsave" },
		verbose = false,
		version = "latest",
		adviser = function(fennel)
			local dir = vim.fn.stdpath "config" .. "/macros"
			local path = string.format(";%s/?.fnl;%s/?/init.fnl", dir, dir)

			fennel["macro-path"] = fennel["macro-path"] .. path

			return fennel
		end,
	}
}
