(import-macros {: command!} :hibiscus.vim)

(let [{: toggle-diagnostics} (require :utils)]
  (command! [] :ToggleDiagnostics toggle-diagnostics))
