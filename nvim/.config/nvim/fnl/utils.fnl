(local M {})

(lambda M.getenv-bool [env]
  (match (string.lower (or (os.getenv env) :false))
    :true  true  :1 true
    :false false :0 false))

(lambda M.termux? [] (= (os.execute :is-termux) 0))
(lambda M.ssh?    [] (= (os.execute :is-ssh) 0))
(lambda M.tty?    [] (= (os.execute :is-tty) 0))

(lambda M.disable-semantic-hl []
  (vim.api.nvim_set_hl 0 "@lsp.type.function" {})
  (each [_ group (ipairs (vim.fn.getcompletion "@lsp" "highlight"))]
    (vim.api.nvim_set_hl 0 group {})))

(lambda M.clear-cmd []
  (vim.api.nvim_command "echo ''"))

(lambda M.get-filetypes [?pat]
  (vim.fn.getcompletion (or ?pat "") "filetype"))

(lambda M.toggle-diagnostics []
  (if (vim.diagnostic.is_disabled)
      (vim.diagnostic.enable)
      (vim.diagnostic.disable)))

(lambda M.executable? [cmd]
  (= 1 (vim.fn.executable cmd)))

(lambda M.buffer-empty? [?bufnr]
  (let [bufnr (or ?bufnr (vim.api.nvim_get_current_buf))
        lines (vim.api.nvim_buf_get_lines bufnr 0 -1 false)
        contents (table.concat lines "\n")]
    (not (contents:match "%S"))))

M
