(import-macros {: set! : setlocal! : map! : augroup!} :hibiscus.vim)

(map! [n] "<leader>op" "<cmd>Lazy<cr>"
      "Open Lazy panel")

;; (map! [n] "<leader>mm" "<cmd>make<cr>"
;;       "Run :make")
;; (map! [n] "<leader>mc" "<cmd>make commit<cr>"
;;       "Run :make commit")

(map! [ci] "<c-l>" (λ []
                     (vim.api.nvim_feedkeys "λ" :t false)
                     (vim.cmd.redraw))
      "Input λ character")

(map! [n] "<leader>td" "<cmd>ToggleDiagnostics<cr>"
      "Toggle LSP diagnostics")

(map! [n] "<leader>e" vim.diagnostic.open_float
      "Open diagnostic description")
(map! [n] "[e" vim.diagnostic.goto_prev
      "Goto previous diagnostic")
(map! [n] "]e" vim.diagnostic.goto_next
      "Goto next diagnostic")

(map! [n] "<leader>gc" #(vim.cmd.edit (vim.uv.fs_realpath (vim.fn.stdpath :config)))
      "Open config directory")

(augroup! :ft-keymap
  [[FileType] [html php]
   #(map! [n :buffer] "<localleader>t" #(match vim.o.ft
                                          :html (setlocal! ft :php)
                                          :php  (setlocal! ft :html))
          "Switch filetype (PHP/HTML)")]
  [[FileType] markdown
   (λ []
     (map! [n :buffer] "<localleader>U" "yyp0v$r="
           "H1 underline")
     (map! [n :buffer] "<localleader>u" "yyp0v$r-"
           "H2 underline"))]
  [[TermOpen] *
   (λ []
     (map! [t :buffer] "<m-.>" "<c-\\><c-n>"
           "Change to NORMAL-mode"))]
  [[LspAttach] *
   (λ [event]
     (tset vim.bo event.buf :omnifunc "v:lua.vim.lsp.omnifunc")
     ;; Hibiscus does not support dynamic buffer mapping
     (vim.keymap.set :n "gd" vim.lsp.buf.definition     {:buffer event.buf
                                                         :desc "Goto definition"})
     (vim.keymap.set :n "gD" vim.lsp.buf.declaration    {:buffer event.buf
                                                         :desc "Goto declaration"})
     (vim.keymap.set :n "gi" vim.lsp.buf.implementation {:buffer event.buf
                                                         :desc "Goto implementation"})
     (vim.keymap.set :n "gr" vim.lsp.buf.references     {:buffer event.buf
                                                         :desc "Show references"})
     ;; (when vim.lsp.inlay_hint
     ;;   (vim.lsp.inlay_hint 0 false)
     ;;   (vim.keymap.set :n "<leader>ch" #(vim.lsp.inlay_hint 0) {:buffer event.buf
     ;;                                                            :desc "Toggle inlay hints"}))
     (vim.keymap.set :n "<leader>cr" vim.lsp.buf.rename                  {:buffer event.buf
                                                                          :desc "Rename item"})
     (vim.keymap.set :n "K" vim.lsp.buf.hover                            {:buffer event.buf
                                                                          :desc "Show item details"})
     (vim.keymap.set [:n :v] "<leader>ca" vim.lsp.buf.code_action        {:buffer event.buf
                                                                          :desc "Code actions"})
     (vim.keymap.set :n "<leader>cf" #(vim.lsp.buf.format {:async true}) {:buffer event.buf
                                                                          :desc "Format project (or file?)"}))])
