(import-macros {: map! : set! : setlocal! : set+ : g! : augroup!} :hibiscus.vim)

;; Indentation
(set! shiftwidth 0)
(set! tabstop 4)

;; Line Numbers
(set! relativenumber)
(set! number)
(set! numberwidth 3)

;; Mouse
(set! mouse "nvi")

;; Formatting
(set+ formatoptions "l")
(set! linebreak)

;; Searching
(set! ignorecase)
(set! smartcase)
(set! incsearch)

;; Clarity
(set! splitbelow)
(set! splitright)
(set! helpheight 0)
(set! nowrap)
(set! list)
(set _G.tab-indent-char "│ ")
(set! listchars {:tab _G.tab-indent-char
                 :trail "-"
                 :nbsp "+"})
(set! fillchars {:eob " "
                 :fold " "
                 :foldopen ""
                 :foldsep " "
                 :foldclose ""})
(set! shortmess "ilmnrxsI")
(set! signcolumn "auto:2-4")
(set! colorcolumn "120")
(set! sidescrolloff 2)
(set! scrolloff 4)
(set! cmdheight 1)
(when (= 1 (vim.fn.has :nvim-0.10))
  (set! smoothscroll))

;; Persistence
(set! noswapfile)
(set! undofile)
(set! notimeout)
(set! confirm)
(set! mousemodel :extend)
(set! shada ["!"
             "'50"
             "<50"
             "s100"
             ":50"
             "/50"
             "h"])

(g! mapleader " ")
(g! maplocalleader "\\")

(augroup! :term-opts
  [[TermOpen] *
    #(setlocal! signcolumn "auto")])

(let [{: tty?} (require :utils)
      ;; Normally, NeoVim handles cursor just fine, but in
      ;; TTY, for some reason, block cursor does not behave properly
      set-tty-only-options (λ []
                             (set! guicursor "a:ver100,r-cr-o:hor20"))
      set-gui-only-options (λ []
                             (set! clipboard "unnamedplus")
                             (set! termguicolors)
                             (set! cursorline))]
  (if (tty?)
      (set-tty-only-options)
      (set-gui-only-options)))

(vim.api.nvim_set_hl 0 "@lsp.type.keyword.rust" {})
