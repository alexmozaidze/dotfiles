(import-macros {: set! : setlocal! : map!} :hibiscus.vim)

(map! [n] "<esc>" "<cmd>noh<cr>" "Disable search highlight")

(map! [n :remap] "<leader>w" "<c-w>")
(map! [n] "<leader>qq" "<cmd>qa<cr>")

(map! [n] "<leader>be" "<cmd>e<cr>" "Re-open buffer")
(map! [n] "<leader>bE" "<cmd>e!<cr>" "Forcefully re-open buffer")

(map! [n] "<leader>bs" "<cmd>w<cr>" "Save buffer")
(map! [n] "<leader>bS" "<cmd>w!<cr>" "Forcefully save buffer")

(map! [n] "<leader>bk" "<cmd>bd<cr>" "Delete buffer")
(map! [n] "<leader>bK" "<cmd>%bd<cr>" "Delete all buffers")

(map! [n] "<leader>bm" "<cmd>delm!<cr>" "Delete all marks")

(map! [n] "<leader>n" "bn" "Goto next buffer")
(map! [n] "<leader>p" "bp" "Goto previous buffer")

(map! [n] "<leader>tw" #(setlocal! wrap!) "Toggle 'wrap'")
(map! [n] "<leader>tr" #(set! cursorline!) "Toggle 'cursorline'")
(map! [n] "<leader>tc" #(set! cursorcolumn!) "Toggle 'cursorcolumn'")
(map! [n] "<leader>th" #(set! hlsearch!) "Toggle 'hlsearch'")
(map! [n] "<leader>tm" #(set! modifiable!) "Toggle 'modifiable'")
(map! [n] "<leader>tl" #(set! lazyredraw!) "Toggle 'lazyredraw'")
