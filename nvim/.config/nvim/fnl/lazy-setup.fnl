(local lazy (require :lazy))
(local config {:change_detection {:notify false}
               :install {:colorscheme [:palenight
                                       :habamax]}
               :dev {:path "~/projects"
                     :fallback true}})

(lazy.setup "plugins" config)
