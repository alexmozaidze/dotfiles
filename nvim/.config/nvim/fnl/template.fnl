(lambda list-templates []
  (let [ft vim.o.ft
        invalid-ft? (or (not ft)
                        (= ft ""))]
    (if invalid-ft?
        []
        (vim.tbl_flatten (icollect [_ glob-path (ipairs [(.. :templates/ ft :.template) (.. :templates/ ft :/*.template)])]
                           (vim.fn.globpath vim.o.rtp glob-path false true))))))

(lambda expand-template [template-file]
  (let [file (io.open template-file)
        text (file:read :*a)
        text (text:gsub "\n$" "")
        body (vim.split text "\n")
        snip {: body
              :description ""
              :kind :snipmate
              :prefix ""}
        snippy (require :snippy)]
    (snippy.expand_snippet snip "")))

(local M {})

(lambda M.expand []
  (let [templates (list-templates)
        path->filename #($:match "([^/]+)$")
        filename-strip-extension #($:match "^(.*)%.")
        clear-buffer #(vim.api.nvim_buf_set_lines (vim.api.nvim_get_current_buf) 0 -1 false {})
        expand-template (λ [template]
                          (clear-buffer)
                          (expand-template template))]
    (if (= (length templates) 1)
        (expand-template (. templates 1))
        (< 1 (length templates))
        (vim.ui.select templates
                       {:prompt "Select template to expand:"
                        :format_item #(filename-strip-extension (path->filename $))}
                       (fn [selected]
                         (when (not= selected nil)
                           (expand-template selected)))))))

M
