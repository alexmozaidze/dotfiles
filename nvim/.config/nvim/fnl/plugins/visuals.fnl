(import-macros {: set! : g!} :hibiscus.vim)

[{1 :nvim-lualine/lualine.nvim
  :event :UIEnter
  :init #(set! noshowmode)
  :opts {:options {:component_separators {:left "" :right ""}
                   :section_separators   {:left "" :right ""}}
         :sections {:lualine_x ["require('lsp-progress').progress()"
                                :filetype]
                    :lualine_c [{1 :filename
                                 :path 1}]}}}
 {1 :lukas-reineke/indent-blankline.nvim
  :main :ibl
  :event :UIEnter
  :cmd [:IBLEnable
        :IBLDisable
        :IBLToggle
        :IBLEnableScope
        :IBLToggleScope
        :IBLDisableScope]
  :keys [{1 "<leader>ts" 2 "<cmd>IBLToggleScope<cr>"
          :desc "Toggle Ibl scope"}
         {1 "<leader>ti" 2 (let [listchars-tab-get #(. (vim.opt.listchars:get) :tab)
                                 listchars-tab-set #(vim.opt.listchars:append (.. "tab:" $))
                                 enable-indent (λ []
                                                 (listchars-tab-set _G.tab-indent-char)
                                                 (vim.cmd :IBLEnable))
                                 disable-indent (λ []
                                                  (listchars-tab-set "  ")
                                                  (vim.cmd :IBLDisable))
                                 toggle-indents (λ []
                                                  (if (not= (listchars-tab-get) "  ")
                                                      (disable-indent)
                                                      (enable-indent)))]
                             toggle-indents)
          :desc "Toggle indentation chars"}]
  :opts {:scope {:enabled false
                 :show_start false
                 :show_end false
                 :highlight :Statement}
         :indent {:char "┆"}}}
 {1 :ellisonleao/glow.nvim
  :cmd :Glow
  :keys [{1 "<localleader>p" 2 "<cmd>Glow<cr>"
          :desc "Glow preview"}]
 :opts {}}
 {1 :davidgranstrom/nvim-markdown-preview
  :cmd :MarkdownPreview}
 (let [ft [:css
           :scss
           :sass]]
   {1 :NvChad/nvim-colorizer.lua
    : ft
    :cmd [:ColorizerAttachToBuffer
          :ColorizerDetachFromBuffer
          :ColorizerReloadAllBuffers
          :ColorizerToggle]
    :keys [{1 "<leader>tc" 2 "<cmd>ColorizerToggle<cr>"
            :desc "Toggle colorizer"}]
    :opts {:filetypes ft
           :user_default_options {:names false}}})
 {1 :lewis6991/gitsigns.nvim
  :event :UIEnter
  :opts {}}
 {1 :chentoast/marks.nvim
  :event :UIEnter
  :opts {}
  :config (λ [_ config]
            (let [marks (require :marks)]
              (marks.setup config)
              (vim.api.nvim_set_hl 0 "MarkSignNumHL" {})))}
 {1 :folke/trouble.nvim
  :cmd :Trouble
  :keys [{1 "<leader>xx" 2 "<cmd>Trouble diagnostics<cr>"
          :desc "Toggle diagnostics report window"}]
  :opts {}}
 {1 :folke/twilight.nvim
  :cmd [:Twilight
        :TwilightEnable
        :TwilightDisable]
  :keys [{1 "<leader>tt" 2 "<cmd>Twilight<cr>"
          :desc "Toggle dim/focus mode"}]
  :opts {:expand [:function :method :if_statement]}}
 {1 :folke/zen-mode.nvim
  :cmd :ZenMode
  :keys [{1 "<leader>tz" 2 "<cmd>ZenMode<cr>"
          :desc "Toggle zen mode"}]
  :opts {:window {:options {:number false
                            :relativenumber false
                            :cursorline false
                            :cursorcolumn false
                            :signcolumn :no}}}}
 {1 :rcarriga/nvim-notify
  :lazy false
  :keys [{1 "<leader>zc" 2 #((. (require :notify) :dismiss) {:silent true})
          :desc "Clear active notifications"}]
  :opts {:timeout 3000
         :render :wrapped-compact
         :stages (if vim.o.termguicolors
                     :fade
                     :static)}
  :config (λ [_ opts]
            (let [notify (require :notify)]
              (notify.setup opts)
              (set vim.notify notify)))}
 {1 :mawkler/modicator.nvim
  :event :UIEnter
  :opts {:show_warnings false
         :integration {:lualine {:enabled false}}}}
 {1 :Bekaboo/deadcolumn.nvim
  :event :UIEnter
  :opts {:scope :cursor
         :blending {:hlgroup [:Normal :bg]}
         :warning {:hlgroup [:DiagnosticWarn :bg]}}}
 {1 :utilyre/sentiment.nvim
  :event :VeryLazy
  :init #(g! loaded_matchparen 1)
  :opts {}}
 {1 :folke/todo-comments.nvim
  :opts {:highlight {:pattern ".*<((KEYWORDS)%(\\(.{-1,}\\))?):"}}}
 ]
