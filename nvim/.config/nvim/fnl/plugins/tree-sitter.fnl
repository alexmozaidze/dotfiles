(import-macros {: augroup! : set!} :hibiscus.vim)

[{1 :nvim-treesitter/nvim-treesitter
  :build ":TSUpdate"
  :config (λ []
            (set! foldmethod :expr)
            (set! foldexpr "nvim_treesitter#foldexpr()")
            (set! nofoldenable)
            (local tree-sitter (require :nvim-treesitter))
            (local tree-sitter-config (require :nvim-treesitter.configs))
            (local ensure-installed [:fennel
                                     :lua
                                     :vimdoc
                                     :vim
                                     :bash
                                     :html
                                     :markdown
                                     :markdown_inline
                                     :printf
                                     :luap])
            (tree-sitter.setup)
            (tree-sitter-config.setup {:indent {:enable true}
                                       :endwise {:enable true}
                                       :ensure_installed ensure-installed
                                       :auto_install true
                                       :highlight {:enable true}}))}
 :nushell/tree-sitter-nu
 {1 :nvim-treesitter/nvim-treesitter-context
  :dependencies :nvim-treesitter/nvim-treesitter
  :config {:mode :topline
           :line_numbers false
           :max_lines 3}}
 {1 :RRethy/nvim-treesitter-endwise
  :dependencies :nvim-treesitter/nvim-treesitter
  :event [:BufNewFile
          :BufRead]}
 {1 :Wansmer/treesj
  :dependencies :nvim-treesitter/nvim-treesitter
  :keys [{1 "<leader>l" 2 #((. (require :treesj) :toggle))
          :desc "Prettify/inline nested syntax"}]
  :opts {:max_join_length 260
         :use_default_keymaps false}}
 {1 :mizlan/iswap.nvim
  :dependencies :nvim-treesitter/nvim-treesitter
  :dev true
  :cmd [:ISwap
        :ISwapNode
        :ISwapNodeWith
        :ISwapNodeWithLeft
        :ISwapNodeWithRight
        :ISwapWith
        :ISwapWithLeft
        :ISwapWithRight]
  :keys [{1 "<leader>S" 2 "<cmd>ISwap<cr>"
          :desc "Swap around entries"}]
  :opts {}}
 {:url "https://gitlab.com/HiPhish/rainbow-delimiters.nvim"
  :dev true
  :dependencies :nvim-treesitter/nvim-treesitter}
 ;; {1 :alexmozaidze/tree-comment.nvim
 ;;  :dev true
 ;;  :dependencies :nvim-treesitter/nvim-treesitter
 ;;  :event :UIEnter
 ;;  :opts {}}
 {1 :m-demare/hlargs.nvim
  :dependencies :nvim-treesitter/nvim-treesitter
  :opts {}
  :config (λ [_ opts]
            (let [hlargs (require :hlargs)]
              (hlargs.setup opts)
              (augroup! :hlargs
                [[LspAttach] *
                 (λ [args]
                   (when (?. args :data :client_id)
                     (let [bufnr args.buf
                           client-id args.data.client_id
                           client (vim.lsp.get_client_by_id client-id)
                           capabilities client.server_capabilities
                           semantic-token-support? (?. capabilities :semanticTokensProvider :full)]
                       (when semantic-token-support?
                         (hlargs.disable_buf bufnr)))))])))}
 ]
