(import-macros {: augroup! : g! : map!} :hibiscus.vim)

[{1 :romgrk/barbar.nvim
  :event :UIEnter
  :cmd [:BufferPrevious
        :BufferNext
        :BufferMovePrevious
        :BufferMoveNext
        :BufferGoto
        :BufferLast
        :BufferPin
        :BufferClose
        :BufferRestore
        :BufferWipeout
        :BufferCloseAllButCurrent
        :BufferCloseAllButVisible
        :BufferCloseAllButPinned
        :BufferCloseAllButCurrentOrPinned
        :BufferCloseBuffersLeft
        :BufferCloseBuffersRight
        :BufferPick
        :BufferPickDelete
        :BufferOrderByBufferNumber
        :BufferOrderByDirectory
        :BufferOrderByLanguage
        :BufferOrderByWindowNumber
        :BarbarEnable
        :BarbarDisable]
  :keys (let [normal-keys [{1 "<leader>n" 2 "<cmd>BufferNext<cr>"
                            :desc "Goto next buffer"}
                           {1 "<leader>p" 2 "<cmd>BufferPrevious<cr>"
                            :desc "Goto previous buffer"}
                           {1 "<leader>N" 2 "<cmd>BufferMoveNext<cr>"
                            :desc "Move buffer forward"}
                           {1 "<leader>P" 2 "<cmd>BufferMovePrevious<cr>"
                            :desc "Move buffer backward"}
                           {1 "<leader>0" 2 "<cmd>BufferPick<cr>"
                            :desc "Pick buffer"}
                           {1 "<leader><cr>" 2 "<cmd>BufferPin<cr>"
                            :desc "Pin buffer"}]
              goto-keys (fcollect [i 1 9]
                          {1 (.. "<leader>" i) 2 (.. "<cmd>BufferGoto " i "<cr>")
                           :desc (.. "Goto buffer #" i)})]
          (vim.list_extend normal-keys goto-keys))}
 {1 :lambdalisue/suda.vim
  :cmd [:SudaRead
        :SudaWrite]
  :keys [{1 "<leader>bu" 2 "<cmd>SudaWrite<cr>"
          :desc "Sudo save buffer"}
         {1 "<leader>bU" 2 "<cmd>SudaRead<cr>"
          :desc "Sudo re-open buffer"}]}
 {1 :numToStr/Comment.nvim
  :keys [{1 "gc" :mode [:n :v]}
         {1 "gb" :mode [:n :v]}]
  :opts {}
  :config (λ [_ config]
            (let [comm (require :Comment)
                  comm-ft (require :Comment.ft)]
              (comm.setup config)
              (set comm-ft.fennel ";; %s")
              (set comm-ft.kdl ["// %s" "/* %s */"])))}
 {1 :gbprod/cutlass.nvim
  :keys [{1 "x" :mode [:n :v]}
         {1 "X" :mode [:n :v]}
         {1 "d" :mode [:n :v]}
         {1 "D" :mode [:n :v]}
         ;; {1 "s" :mode [:n :v]}
         ;; {1 "S" :mode [:n :v]}
         {1 "c" :mode [:n :v]}
         {1 "C" :mode [:n :v]}]
  :opts {:cut_key "x"
         :exclude ["ns" "nS"]
         :registers {:change "c"
                     :delete "d"
                     :select "s"}}}
 {1 :akinsho/toggleterm.nvim
  :cmd [:ToggleTerm
        :ToggleTermSendCurrentLine
        :ToggleTermSendVisualLines
        :ToggleTermSendVisualSelection
        :ToggleTermSetName
        :ToggleTermToggleAll]
  :keys [{1 "<m-,>" 2 "<cmd>exe v:count1 .. 'ToggleTerm'<cr>"
          :mode [:n :t]}
         "<leader>gg"]
  :opts {:shade_terminals false
         :float_opts {:border :rounded
                      :title_pos :center}}
  :config (λ [_ opts]
            (let [toggleterm (require :toggleterm)]
              (toggleterm.setup opts))
            (let [{: Terminal} (require :toggleterm/terminal)
                  lazygit (Terminal:new {:cmd "lazygit"
                                         :hidden true
                                         :direction :float
                                         :display_name "LazyGit"
                                         :count 100
                                         ;; :on_open #(map! [t :buffer] "q" "<cmd>close<cr>")
                                         })]
              (map! [n] "<leader>gg" #(lazygit:toggle))))}
 {1 :altermo/ultimate-autopair.nvim
  :event :InsertCharPre
  :opts {:cmap false
         :cr {:addsemi {}
              :conf {:cond #(not (or ($.in_lisp) (= vim.o.ft "clojure")))}}
         ;; Custom pairs
         1 {1 "{%" 2 "%}"
            :ft [:html :markdown]
            :space {:enable true}}
         2 {1 "{#" 2 "#}"
            :ft [:html :markdown]
            :space {:enable true}}
         3 {1 "<!--" 2 "-->"
            :ft [:html :markdown :vue :svelte]
            :space {:enable true}}}}
 {1 :yutkat/confirm-quit.nvim
  :cmd [:ConfirmQuit
        :ConfirmQuitAll]
  :keys [{1 "<leader>qq" 2 "<cmd>ConfirmQuitAll<cr>"
          :desc "Confirm quit"}]
  :opts {:overwrite_q_command false}}
 {1 :kylechui/nvim-surround
  :keys [{1 "<c-g>s" :mode :i}
         {1 "<c-g>S" :mode :i}
         {1 "<leader>s" :mode :v}
         {1 "gS" :mode :v}
         "ys"
         "yss"
         "yS"
         "ySS"
         "cs"
         "cS"
         "ds"]
  :opts {:keymaps {:visual "<leader>s"}}}
 {1 :ofirgall/open.nvim
  :keys [{1 "go" 2 #((. (require :open) :open_cword))
          :desc "Open link under the cursor"}]
  :opts {:curl_opts {:timeout :3000}}}
 {1 :mattn/emmet-vim
  :keys [{1 "<c-z>" :mode :i}]
  :init #(g! user_emmet_leader_key "<C-Z>")}
 {1 :stevearc/oil.nvim
  :lazy false
  :keys [{1 "<leader>-" 2 "<cmd>Oil<cr>" :desc "Open directory"}]
  :opts (let [columns [:icon
                       :permissions
                       :size]]
          {: columns
           :view_options {:show_hidden true}
           :skip_confirm_for_simple_edits true
           :win_options {:signcolumn :auto
           :conceallevel 0
           :wrap true}
           :keymaps {:gd {:desc "Toggle detail view"
           :callback (λ []
                        (let [oil (require :oil)
                                  config (require :oil.config)]
                          (if (= 0 (length config.columns))
                              (oil.set_columns columns)
                              (oil.set_columns []))))}}})}
 {1 :nvim-neorg/neorg
  :tag :v9.1.1
  :ft :norg
  :cmd :Neorg
  :keys [{1 "<leader>gn" 2 "<cmd>Neorg workspace notes<cr>"
          :desc "Open notes"}]
  :opts {:load {:core.defaults {}
                :core.completion {:config {:engine :nvim-cmp}}
                :core.export {}
                :core.export.markdown {:config {:extensions [:todo-items-basic]}}
                ;; :core.concealer {:config {:icon_preset :diamond
                ;;                           :init_open_folds :never}}
                :core.dirman {:config {:workspaces {:notes "~/notes"}}}}}}
 {1 :cappyzawa/trim.nvim
  :event :BufWritePre
  :cmd [:Trim
        :TrimToggle]
  :keys [{1 "<leader>bt" 2 "<cmd>Trim<cr>"
          :desc "Trim buffer"}]
  :opts {:ft_blocklist [:markdown]}}
 {1 :jiaoshijie/undotree
  :keys [{1 "<leader>u" 2 #((. (require :undotree) :toggle))
          :desc "Toggle undotree"}]
  :opts {}}
 {1 :NMAC427/guess-indent.nvim
  :event [:BufNewFile
          :BufRead]
  :cmd :GuessIndent
  :opts {:filetype_exclude [:netrw
                            :tutor
                            :oil
                            :lisp
                            :fennel
                            :clojure
                            :janet]}}
 {1 :nguyenvukhang/nvim-toggler
  :keys ["<leader>i"]
  :opts {:inverses {"1"  "0"
                    "+"  "-"
                    "*"  "/"}}}
 {1 :famiu/bufdelete.nvim
  :cmd [:Bdelete :Bwipeout]
  :keys [{1 "<leader>bk" 2 "<cmd>Bdelete<cr>"
          :desc "Close buffer"}
         {1 "<leader>bK" 2 "<cmd>%Bdelete<cr>"
          :desc "Close all buffers"}]}
 {1 :ggandor/leap.nvim
  :lazy false ; It lazy-loads by itself
  :keys [{1 "s" 2 "<Plug>(leap-forward-to)"
          :mode [:n :v]
          :desc "Leap forward"}
         {1 "S" 2 "<Plug>(leap-backward-to)"
          :mode [:n :v]
          :desc "Leap backwards"}
         {1 "gs" 2 "<Plug>(leap-from-window)"
          :desc "Leap windows"}]}
 {1 :vxpm/ferris.nvim
  :ft "rust"
  :opts {}}
 {1 :cbochs/portal.nvim
  :cmd :Portal
  :keys [{1 "<leader><c-o>" 2 "<cmd>Portal jumplist backward<cr>"
          :desc "Jump backwards in the jumplist"}
         {1 "<leader><c-i>" 2 "<cmd>Portal jumplist forward<cr>"
          :desc "Jump forward in the jumplist"}
         {1 "<leader>;" 2 "<cmd>Portal changelist backward<cr>"
          :desc "Jump backwards in the changelist"}
         {1 "<leader>," 2 "<cmd>Portal changelist forward<cr>"
          :desc "Jump forward in the changelist"}]
  :opts {:labels (vim.split "sfnjklhodweimbuyvrgtaqpcxz/SFNJKLHODWEIMBUYVRGTAQPCXZ?" "")}}
 {1 :stevearc/resession.nvim
  :event [:VimLeavePre
          :VeryLazy]
  :keys [{1 "<leader>ss" 2 #((. (require :resession) :save))}
         {1 "<leader>sl" 2 #((. (require :resession) :load))}
         {1 "<leader>sd" 2 #((. (require :resession) :delete))}]
  :init (λ []
          (augroup! :resession
                    [[VimLeavePre] *
                     #((. (require :resession) :save) "last" {:notify false})]))
  :opts {:autosave {:enabled true
                    :notify false}}}
 {1 :nvim-neo-tree/neo-tree.nvim
  :cmd :Neotree
  :keys [{1 "<leader><tab>" 2 "<cmd>Neotree<cr>"}]}
 {1 :chrisgrieser/nvim-genghis
  :cmd :Genghis
  :keys [{1 "<leader>br" 2 "<cmd>Genghis renameFile<cr>"
          :desc "Rename file"}
         {1 "<leader>bx" 2 "<cmd>Genghis chmodx<cr>"
          :desc "Grant execution permission to file"}
         {1 "<leader>bd" 2 "<cmd>Genghis duplicateFile<cr>"
          :desc "Duplicate buffer"}]
  :opts {:backdrop {:blend 85}}}
 :LudoPinelli/comment-box.nvim
 :mateuszwieloch/automkdir.nvim
 {1 :Olical/conjure
  :ft :clojure}
 :hylang/vim-hy
 ]
