[{1 :nvim-telescope/telescope.nvim
  :dependencies [{1 :nvim-telescope/telescope-fzf-native.nvim
                  :build :make
                  :cond #(if (= (vim.fn.executable :make) 1) true false)}
                 :nvim-telescope/telescope-file-browser.nvim
                 :xiyaowong/telescope-emoji.nvim
                 :nvim-telescope/telescope-symbols.nvim
                 :ghassan0/telescope-glyph.nvim]
  :cmd :Telescope
  :keys [{1 "<leader><leader>" 2 #((. (require :telescope.builtin) :fd) {:hidden true})
          :desc "Find files"}
         {1 "<leader>f." 2 #((. (require :telescope) :extensions :file_browser :file_browser))
          :desc "Open file browser"}
         {1 "<leader>fp" 2 #((. (require :telescope.builtin) :fd) {:search_dirs [(.. (os.getenv :HOME) "/dotfiles/nvim/.config/nvim")]})
          :desc "Find files [NeoVim config]"}
         {1 "<leader>fP" 2 #((. (require :telescope.builtin) :fd) {:search_dirs [(.. (os.getenv :HOME) "/dotfiles")] :hidden true})
          :desc "Find files [dotfiles]"}
         {1 "<leader>fl" 2 #((. (require :telescope.builtin) :highlights))
          :desc "Find highlights"}
         {1 "<leader>fk" 2 #((. (require :telescope.builtin) :keymaps))
          :desc "Find keymaps"}
         {1 "<leader>fg" 2 #((. (require :telescope.builtin) :live_grep))
          :desc "Live grep CWD"}
         {1 "<leader>fb" 2 #((. (require :telescope.builtin) :buffers))
          :desc "Find buffers"}
         {1 "<leader>fo" 2 #((. (require :telescope.builtin) :oldfiles))
          :desc "Find oldfiles"}
         {1 "<leader>fh" 2 #((. (require :telescope.builtin) :help_tags))
          :desc "Select help page"}
         {1 "<leader>fm" 2 #((. (require :telescope.builtin) :marks))
          :desc "Goto mark"}
         {1 "<leader>ff" 2 #((. (require :telescope.builtin) :filetypes))
          :desc "Select filetype"}
         {1 "<leader>fe" 2 #((. (require :telescope) :extensions :emoji :emoji))
          :desc "Find emojis"}
         {1 "<leader>fE" 2 #((. (require :telescope) :extensions :glyph :glyph))
          :desc "Find glyphs"}]
  :opts {:defaults {:mappings {:i {:<c-b> "preview_scrolling_up"
                                   :<c-d> false
                                   :<c-f> "preview_scrolling_down"
                                   :<c-u> false
                                   :<esc> "close"
                                   :<c-h> #(vim.cmd.stopinsert)}}
                    :file_ignore_patterns [".git"]}
         :extensions {:fzf {:fuzzy true
                            :override_file_sorter true
                            :override_generic_sorter true}}}
  :config (λ [_ config]
            (local telescope (require :telescope))
            (telescope.load_extension :emoji)
            (telescope.load_extension :glyph)
            (telescope.load_extension :fzf)
            (telescope.setup config))}]
