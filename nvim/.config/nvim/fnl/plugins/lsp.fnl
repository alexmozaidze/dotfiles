[{1 :williamboman/mason.nvim
  :build ":MasonUpdate"
  :dependencies [{1 :williamboman/mason-lspconfig.nvim
                  :opts {}}
                 {1 :lukas-reineke/lsp-format.nvim
                  :opts {}}
                 {1 :nvimtools/none-ls.nvim
                  :opts {}}
                 {1 :jay-babu/mason-null-ls.nvim
                  :opts {:automatic_setup true
                         :handlers {}}}
                 :neovim/nvim-lspconfig]
  ;; NOTE: We need to load before the buffers since we want to spin up an LSP
  ;; depending on buffer's filetype
  :event [:VeryLazy
          :BufNewFile
          :BufRead]
  :cmd [:Mason
        :MasonInstall
        :MasonLog
        :MasonUninstall
        :MasonUninstallAll
        :MasonUpdate]
  :config (λ []
            (let [ns (vim.api.nvim_create_namespace :signs-namespace)
                  original-signs-handler vim.diagnostic.handlers.signs
                  show (λ [_ bufnr _ opts]
                          (let [diagnostics (vim.diagnostic.get bufnr)
                                max-severity-per-line (accumulate [max-severity-per-line {}
                                                                   _ d (pairs diagnostics)]
                                                        (let [m (. max-severity-per-line d.lnum)]
                                                          (when (or (not m) (< d.severity m.severity))
                                                            (tset max-severity-per-line d.lnum d))
                                                          max-severity-per-line))
                                filtered-diagnostics (vim.tbl_values max-severity-per-line)]
                            (original-signs-handler.show ns bufnr filtered-diagnostics opts)))
                  hide (λ [_ bufnr]
                         (original-signs-handler.hide ns bufnr))]
              (set vim.diagnostic.handlers.signs {: show : hide}))
            (vim.cmd "sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl=")
            (vim.cmd "sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=")
            (vim.cmd "sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl=")
            (vim.cmd "sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl=")
            (let [{: termux?} (require :utils)
                  mason (require :mason)
                  mason-lspconfig (require :mason-lspconfig)
                  lspconfig (require :lspconfig)]
              (mason.setup {})
              (let [lua-globals [:vim
                                 :love
                                 :inspect
                                 :case]
                    {:on_attach autoformat} (require :lsp-format)
                    lsp-default-options (let [{:default_capabilities cmp-completion} (require :cmp_nvim_lsp)]
                                          {:capabilities (cmp-completion)})
                    lsp-options {1 #((. lspconfig $ :setup) lsp-default-options)
                                 :fennel_language_server {:settings {:fennel {:diagnostics {:globals lua-globals}
                                                                              :workspace {:library (vim.api.nvim_list_runtime_paths)}}}}
                                 :lua_ls {:settings {:Lua {:diagnostics {:globals lua-globals}
                                                     :runtime {:version :LuaJIT}
                                                     :telemetry {:enable false}
                                                     :workspace {:library (vim.api.nvim_get_runtime_file "" true)}}}}
                                 :yamlls {:settings {:yaml {:keyOrdering false}}}
                                 :html {:filetypes [:html :njk]}
                                 :tailwindcss {:filetypes [:svelte :html :vue :css]}
                                 :gopls {:on_attach (λ [client bufnr]
                                                      (autoformat client bufnr))
                                         :hints {:assignVariableTypes true
                                                 :compositeLiteralFields true
                                                 :constantValues true
                                                 :parameterNames true}
                                         :semanticTokens true}}
                    lsp-handlers (collect [name config (pairs lsp-options)]
                                   (if (= :number (type name))
                                       (values name config)
                                       (let [lsp-setup (. lspconfig name :setup)
                                             new-lsp-options (vim.tbl_deep_extend :force lsp-default-options config)]
                                         (values name #(lsp-setup new-lsp-options)))))]
                (when (termux?)
                  (lspconfig.lua_ls.setup lsp-options.lua_ls)
                  (lspconfig.texlab.setup (or lsp-options.texlab {}))
                  (lspconfig.rust_analyzer.setup (or lsp-options.rust_analyzer {}))
                  (lspconfig.clangd.setup (or lsp-options.clangd {})))
                (mason-lspconfig.setup_handlers lsp-handlers))))}
 {1 :j-hui/fidget.nvim
  :event :LspAttach
  :opts {}}
 {1 :ray-x/lsp_signature.nvim
  :event :LspAttach
  :opts {:hint_prefix "=> "
         :handler_opts {:border :rounded}
         :toggle_key "<C-f>"
         :floating_window false}}
 ]
