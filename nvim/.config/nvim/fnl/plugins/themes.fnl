[{1 :alexmozaidze/palenight.nvim
  :dev true
  :priority 2000
  :config (λ []
            (let [{: termux?} (require :utils)
                  palenight (require :palenight)
                  italic (or vim.g.italic
                             (not (termux?)))]
              (palenight.setup {: italic})))}]
