(import-macros {: map!} :hibiscus.vim)

[{1 :hrsh7th/nvim-cmp
    :dependencies [:hrsh7th/cmp-nvim-lsp
                   :hrsh7th/cmp-buffer
                   :hrsh7th/cmp-path
                   :hrsh7th/cmp-cmdline
                   :dcampos/nvim-snippy
                   :dcampos/cmp-snippy
                   :onsails/lspkind.nvim]
    :event :InsertEnter
    :config (λ []
               (local snippy (require :snippy))
               (local cmp (require :cmp))
               (local lspkind (require :lspkind))
               (cmp.setup {:formatting {:format (lspkind.cmp_format {:ellipsis_char "..."
                                                                     :maxwidth 50
                                                                     :mode :symbol})}
                           :mapping (cmp.mapping.preset.insert {:<c-Space> (cmp.mapping.complete)
                                                                :<c-b> (cmp.mapping.scroll_docs -4)
                                                                :<c-e> (cmp.mapping.abort)
                                                                :<c-f> (cmp.mapping.scroll_docs 4)
                                                                :<c-s> (cmp.mapping.confirm {:select false})})
                           :snippet {:expand #(snippy.expand_snippet $.body)}
                           :sources (cmp.config.sources [{:name :snippy}
                                                         {:name :nvim_lsp}]
                                                        [{:name :buffer}])})
               (let [mappings (require :snippy.mapping)]
                 (map! [i] "<tab>" (mappings.expand_or_advance "<Tab>"))
                 (map! [s] "<tab>" (mappings.next "<Tab>"))
                 (map! [is] "<s-tab>" (mappings.previous "<S-Tab>"))
                 (map! [x :remap] "<tab>" mappings.cut_text)
                 (map! [n :remap] "g<tab>" mappings.cut_text)))}]
