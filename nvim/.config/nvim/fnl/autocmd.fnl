(import-macros {: setlocal! : set! : augroup!} :hibiscus.vim)

(augroup! :ft-indent
  [[FileType]
   [vim
    html
    astro
    tex
    htmx
    php
    vue
    json
    jsonc
    json5
    hjson
    yaml
    yml
    toml
    css
    scss
    ass]
   #(setlocal! tabstop 2)]
  ;; 3 space indentation lines up nicely with `if` statements
  [[FileType] [lua go] #(setlocal! tabstop 3)]
  [[FileType] [lisp markdown] #(setlocal! expandtab)])

(augroup! :ft-opt
  [[FileType]
   [markdown norg]
   (λ []
     (setlocal! wrap)
     (setlocal! colorcolumn :0))]
  [[FileType]
   [fennel lisp]
   #(set! commentstring ";;%s")]
  [[FileType] janet #(set! lisp)]
  [[FileType] snippets #(setlocal! nofoldenable)])

(augroup! :adaptive-cmd-height
  [[VimEnter VimResized] *
   #(set! cmdheight (if (< 80 vim.o.columns) 1 2))])

(augroup! :highlight-yanked-text
  [[TextYankPost] *
   #(vim.highlight.on_yank {:higroup "Visual"
                            :timeout 350})])

(let [template (require :template)
      {: buffer-empty?} (require :utils)]
  (augroup! :cmp-file-templates
    [[BufNewFile BufRead] * #(if (buffer-empty?)
                                 (template.expand))]))

(augroup! :term-stuff
  [[TermOpen] *
   (λ []
     (setlocal! nonumber)
     (setlocal! norelativenumber))])
