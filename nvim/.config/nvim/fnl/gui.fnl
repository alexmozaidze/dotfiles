(import-macros {: set! : g!} :hibiscus.vim)

(when vim.g.neovide
  (set! guifont ["Iosevka:h14"])
  (g! neovide_remember_window_size true)
  (g! neovide_scale_factor 0.9)
  (g! neovide_hide_mouse_when_typing true)
  (g! neovide_cursor_animation_length 0.10)
  (g! neovide_cursor_trail_size 0.1)
  (g! neovide_cursor_antialiasing false)
  (g! neovide_cursor_unfocused_outline_width 0.05)
  (g! neovide_scroll_animation_length 0.6))
