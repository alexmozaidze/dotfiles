(import-macros {: set! : color! : g!} :hibiscus.vim)

(local {: ssh?} (require :utils))

(require :gui)
(require :lazy-bootstrap)
(require :defaults)
(require :keymaps)
(require :lazy-setup)
(require :commands)
(require :autocmd)
(require :override-keymaps)

(if (ssh?) (set! lazyredraw))
(if _G.nogui (set! notermguicolors))

(when (= 1 vim.g.shell_edit)
  (set! noundofile)
  (set! signcolumn :auto))

(vim.filetype.add {:extension {:njk :html}})
(vim.filetype.add {:extension {:webc :html}})
(vim.treesitter.language.register "bash" ["sh" "bash" "zsh"])

(g! zig_fmt_autosave 0)

(color! :palenight)
