; extends

; Tangerine macros
(list
  call: (symbol) @function.macro
  (#not-eq? @function.macro "tail!")
  (#lua-match? @function.macro "!$"))

(list
  call: (symbol) @function.macro
  (#any-of? @function.macro "set+" "set^"))

(list
  call: (symbol) @function.macro
  (#any-of? @function.macro "set!" "setlocal!" "setglobal!")
  item: (symbol) @operator @variable
  (#lua-match? @variable "!$")
  (#offset! @variable 0 0 0 -1))

(list
  call: (symbol) @function.macro
  (#eq? @function.macro "map!")
  .
  item: (sequence
          .
          item: (symbol) @character.special))

; BUG: Although the query is correct, Neovim's highlighting flickers because
; it takes some time for it to process the query and highlight, making it unreliable
; and annoying.
; (list
;   call: (symbol) @function.macro
;   (#eq? @function.macro "augroup!")
;   item: (sequence
;           .
;           item: (sequence
;                   item: (symbol) @constant)
;           item: [
;                   ((symbol) @string)
;                   (multi_symbol
;                     _ @string)
;                   (sequence
;                     (symbol) @string)
;                 ]))
