# Codecs

- `-c copy` → copy all streams
- `-c:{a,v,s} copy` → copy audio/video/subtitle stream(s)

**NOTE:** you can use a codec in place of `copy`

# Mapping

- `-map 0` → map all streams
- `-map 0:{a,v,s}` → map all audio/video/subtitle streams
- `-map 0:{a,v,s}:i` → map specified audio/video/subtitle stream from stream i
- `-map_metadata 0` → map all metadata

For excluding streams, negate the first 0. Here's an
example to remove the first subtitles stream: `-map -0:s:0`

**NOTE:** the exclusion must go after all other maps to take
effect.

# Cutting/Trimming

- `-ss hh:mm:ss` (before `-i`) → approximate time seek
- `-ss hh:mm:ss` (after `-i`) → accurate time seek
- `-t hh:mm:ss` (after `-ss`) → duration of output video

**NOTE:** using `-c:v copy` may result in choppy and/or out of sync audio, along with a second of video pause at the start

# Dumb Players

Some players need a specific color space format to play h264 videos properly. In order to achieve this, use `-vf format=yuv420p`
