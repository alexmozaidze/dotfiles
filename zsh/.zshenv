function path_prepend {
	for p in "$@"; do
		export PATH="$p:$PATH"
	done
}
function path_append {
	for p in "$@"; do
		export PATH="$PATH:$p"
	done
}

path_prepend \
	"$HOME/.zig-bin" \
	"$HOME/.local/share/pipx/venvs" \
	"$HOME/.cargo/bin" \
	"$HOME/.bun/bin" \
	"$HOME/go/bin" \
	"$HOME/.local/share/bob/nvim-bin" \
	"$HOME/.local/bin" \
	"$HOME/.local/scripts" \
	"$HOME/.deno/bin" \
	"/usr/local/go/bin"

if [[ -n "$PREFIX" ]]; then
	path_prepend "$PREFIX/opt/bin"
	export XDG_RUNTIME_DIR="$PREFIX/tmp"
fi

[[ -s "/home/alex/.bun/_bun" ]] && source "/home/alex/.bun/_bun"

if command-exists luarocks; then
	eval "$(luarocks path --bin)"
fi

export EDITOR="nvim"
export VISUAL="$EDITOR"

if is-termux; then
	export DOWNLOADS="$HOME/storage/downloads"
	export VIDEOS="$HOME/storage/movies"
	export MUSIC="$HOME/storage/music"
else
	export DOWNLOADS="$HOME/Downloads"
	export VIDEOS="$HOME/Videos"
	export MUSIC="$HOME/Music"
fi

[[ -f "$HOME/.config/aliasrc" ]] && source "$HOME/.config/aliasrc"
. "$HOME/.cargo/env"
